Сервис загружает списки экстремистов и стоп-списки. И возвращает результаты запроса 
по этим спискам

По умолчанию папки для загрузки:
\input\extremist - для xlsx файлов
\input\extremist\dbf - для dbf файлов

\input\stop_list - для xlsx файлов

Загруженные файлы сначала переформируются в json и затем загружаются в elasticsearch.
Файлы json лежат в папке json.
Загруженные обработанные файлы лежат в processed.

Настройки в application.yml:

####Порт сервиса
server.port: 8090

#####Папки, куда класть dbf и получать json
folder:
  input:
    extremist: input/extremist
    stop_list: input/stop_list

####Кол-во записей в json файле
record.count: 10000

#####Для теста. Показывает, что система работает
greeting: Привет!
timer:
  period: 2000

#####Адресс Elasticsearch
es:
  url: http://localhost:9200

####Индекс и тип для экстремистов. Для запросов.
terror:
  alias: terror_index
  type: terror

####Индекс и тип для стоп-листа. Для запросов.
stop_list:
  alias: stop_list_index
  type: stop_list