package com.farzoom.uni.common.api.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class SearchStopListResponse {

    @JsonProperty("stop_list_id")
    private String stopListId;
    @JsonProperty("client_id")
    private String clientId;
    @JsonProperty("inn")
    private String inn;
    @JsonProperty("kpp")
    private String kpp;
    @JsonProperty("full_name_rus")
    private String fullNameRus;
    @JsonProperty("short_name_rus")
    private String shortNameRus;
    @JsonProperty("full_name_en")
    private String fullNameEn;
    @JsonProperty("short_name_en")
    private String shortNameEn;
    @JsonProperty("okpo")
    private String okpo;
    @JsonProperty("ogrn")
    private String ogrn;
    @JsonProperty("okfs")
    private String okfs;
    @JsonProperty("country")
    private String country;
    @JsonProperty("is_resident")
    private String isResident;
    @JsonProperty("inn_not_resident")
    private String innNotResident;
    @JsonProperty("stop_list_type")
    private String stopListType;
    @JsonProperty("is_terrorist")
    private String isTerrorist;
    @JsonProperty("file_id")
    private String fileId;
    @JsonProperty("_scope")
    Double scope;
}
