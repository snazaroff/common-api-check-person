package com.farzoom.uni.common.api.service;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

@Slf4j
@Service
public class LoadExtremistXlsService extends LoadService {

    private final String inputFolder;
    private final String outputFolder;
    private final int maxRecordCount;

    public LoadExtremistXlsService(@Value("${folder.input.extremist}") String inputFolder,
                                   @Value("${record.count:1000}") int maxRecordCount, @Value("${es.url}") String esUrl,
                                   @Value("${terror.alias}") String alias, @Value("${terror.type}") String type) {
        super(esUrl, alias, type);
        this.inputFolder = inputFolder;
        this.outputFolder = inputFolder + "/json";
        this.maxRecordCount = maxRecordCount;
    }

    public void process(String fileName) throws IOException {
        log.debug("process(): " + fileName);

        long start = System.currentTimeMillis();
        File f = new File(outputFolder);
        if (!f.exists())
            f.mkdirs();

        String timeStamp = (new SimpleDateFormat("yyyy-MM-dd-hh-mm-ss")).format(new Date());

        final SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");

        StringBuffer result = new StringBuffer();

        try(InputStream in = new FileInputStream(new File(fileName))) {

            XSSFWorkbook wb = new XSSFWorkbook(in);

            ObjectMapper om = new ObjectMapper();

            Sheet sheet = wb.getSheetAt(0);
            int rowCount = sheet.getLastRowNum();
            Iterator<Row> it = sheet.iterator();
            int rowNum = 0;
            int recordCount = 0;
            LoadData prevData = null;
            while (it.hasNext()) {
                rowNum++;
                Row row = it.next();
                if (rowNum < 2) continue;

                LoadData data = new LoadData();
                for (int i = 0; i < row.getLastCellNum(); i++) {
                    Cell cell = row.getCell(i);
                    if (cell == null)
                        continue;
                    String value;
                    if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
                        value = new String(cell.getStringCellValue().getBytes("cp1251")).trim();
                    } else if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
                        value = Integer.toString((int) cell.getNumericCellValue());
                    } else if (cell.getCellType() == Cell.CELL_TYPE_BLANK) {
                        value = "";
                    } else {
                        throw new RuntimeException("Wrong cell type: " + cell.getCellType());
                    }

                    if (value == null || value.equals("")) continue;
                    switch (i) {
                        //    type: TU, //1 - ФЛ, 3 - ЮЛ. !!! 0 -> продолжение предыдущей строки
                        case 2:
                            data.setType(Integer.valueOf(value).intValue());
                            break;
                        //    names: NAMEU,
                        case 3:
                            data.setName(value);
                            break;
                        //    country: KODCN,
                        case 6:
                            data.setCountry(value);
                            break;
                        //    address: ADRESS,
                        case 8:
                            data.setAddress(value);
                            break;
                        //    passport:{series: SD, number: RG},
                        case 10:
                            data.setPassportSeries(value);
                            break;
                        //    passport:{series: SD, number: RG},
                        case 11:
                            data.setPassportNumber(value);
                            break;
                        //    birthDate: GR,
                        case 14:
                            if (value != null && value.equals(""))
                                try {
                                    data.setBirthDate(sdf.parse(value));
                                } catch (ParseException pe) {
                                    log.error(pe.getMessage(), pe);
                                }
                            break;
                        //    birthPlace: MR,
                        case 16:
                            data.setBirthPlace(value);
                            break;
                        //    inn: ND
                        case 12:
                            data.setInn(value);
                            break;
                    }
                }
                if (prevData != null) {
                    if (data.getType() == 0) {
                        prevData.merge(data);
                    } else {
                        result.append("{\"index\":{\"_id\": null}}");
                        result.append("\n");
                        result.append(om.writeValueAsString(prevData));
                        result.append("\n");
                        prevData = data;
                        recordCount++;
                    }
                } else {
                    prevData = data;
                }

                if (recordCount == maxRecordCount) {
                    String filePath = outputFolder + "/" + timeStamp + "." + UUID.randomUUID().toString() + ".json";
                    try (FilterOutputStream os = new PrintStream(new FileOutputStream(new File(filePath)), true, "UTF-8")) {
                        os.write(result.toString().getBytes("UTF-8"));
                    } catch (Exception e) {
                        log.error(e.getMessage(), e);
                    }
                    processJsonToEs(filePath);
                    result = new StringBuffer();
                    recordCount = 0;
//                    break;
                }
//                break;
            }

            //Сохраняем данные в файл
            String filePath = outputFolder + "/" + timeStamp + "." + UUID.randomUUID().toString() + ".json";
            try (FilterOutputStream os = new PrintStream(new FileOutputStream(new File(filePath)), true, "UTF-8")) {
                os.write(result.toString().getBytes("UTF-8"));
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
            processJsonToEs(filePath);

            File processed = new File(inputFolder + "/processed/");
            if (!processed.exists())
                processed.mkdirs();

            Files.move(Paths.get(fileName), Paths.get(inputFolder + "/processed/" + new File(fileName).getName()), REPLACE_EXISTING);

            removeOldIndex(alias, alias + "-" + timeStamp);
        }

        log.debug("Load: " + fileName + "; time: " + (System.currentTimeMillis() - start) + " ms");
    }

    @Setter
    @Getter
    @ToString
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private class LoadData {

//    type: TU, //1 - ФЛ, 3 - ЮЛ. !!! 0 -> продолжение предыдущей строки
//    names: NAMEU,
//    country: KODCN,
//    address: ADRESS,
//    passport:{series: SD, number: RG},
//    birthDate: GR,
//    birthPlace: MR,
//    inn: ND

        @JsonProperty("type")
        Integer type;
        @JsonProperty("name")
        String name;
        @JsonProperty("country")
        String country;
        @JsonProperty("address")
        String address;
        @JsonProperty("passportSeries")
        String passportSeries;
        @JsonProperty("passportNumber")
        String passportNumber;
        @JsonProperty("birthDate")
        @JsonFormat(shape= JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
        Date birthDate;
        @JsonProperty("birthPlace")
        String birthPlace;
        @JsonProperty("inn")
        String inn;

        public void merge(LoadData data) {
            if (data.name != null && !data.name.equals(""))
                this.name = this.name.concat(data.name);
        }
    }
}