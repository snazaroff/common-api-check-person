package com.farzoom.uni.common.api.service;

import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;

@Slf4j
@Service
abstract class LoadService {

    protected final String esUrl;
    protected final String alias;
    protected final String type;
    protected final RestTemplate restTemplate;

    public LoadService(String esUrl, String alias, String type) {
        this.esUrl = esUrl;
        this.alias = alias;
        this.type = type;
        this.restTemplate = new RestTemplate();
    }

    abstract void process(String fileName) throws IOException;

    /**
     * Загружаем json в elastic
     * @param fileName - имя файла
     * @throws IOException
     */
    protected void processJsonToEs(String fileName) throws IOException {
        log.debug("LoadStopListService.processJsonToEs(): " + fileName);
        File file = new File(fileName);
        String indexName = alias + "-" + file.getName().substring(0, 19);

        InputStream is = new FileInputStream(file);
        String dataString = new String(IOUtils.toByteArray(is), "UTF-8");
        is.close();

        HttpHeaders headers = new HttpHeaders();
        MediaType mediaType = new MediaType("application", "json", StandardCharsets.UTF_8);
        headers.setContentType(mediaType);

        try {
            log.debug(esUrl + "/" + indexName + "/_mapping/" + type);
            ResponseEntity<String> mappingEntity = restTemplate.getForEntity(esUrl + "/" + indexName + "/_mapping/" + type, String.class);
        } catch(HttpClientErrorException e) {
            if (e.getStatusCode() == HttpStatus.NOT_FOUND) {
                log.debug("mapping: " + e.getMessage());
                ClassLoader classloader = Thread.currentThread().getContextClassLoader();
                InputStream mIs = classloader.getResourceAsStream("mapping." + type + ".json");

                String mappingString = IOUtils.toString(mIs);
                mappingString = mappingString.replace("%type%", type);
                mIs.close();

                HttpEntity<String> mappingRequest = new HttpEntity<>(mappingString, headers);
                ResponseEntity<String> mappingEntity = restTemplate.postForEntity(esUrl + "/" + indexName, mappingRequest, String.class);

                //Set aliases
                mIs = classloader.getResourceAsStream("alias.json");

                String aliasString = IOUtils.toString(mIs);
                aliasString = aliasString.replace("%alias%", alias).replace("%index%", type).replace("%new_index%", indexName);
                IOUtils.write(aliasString, new FileOutputStream(new File("tmp.txt")));
                mIs.close();
                HttpEntity<String> aliasRequest = new HttpEntity<>(aliasString, headers);
                ResponseEntity<String> aliasEntity = restTemplate.postForEntity(esUrl + "/_aliases", aliasRequest, String.class);
            }
        }
        HttpEntity<String> request = new HttpEntity<>(dataString, headers);
        ResponseEntity<String> entity = restTemplate.postForEntity(esUrl + "/" + indexName + "/" + type + "/_bulk", request, String.class);
    }

    protected void removeOldIndex(String prefix, String newIndexName) {
        //http://localhost:9200/terror_index*/_mappings
        ObjectNode arr = restTemplate.getForObject(
                esUrl + "/" + alias + "*/_mappings",
                ObjectNode.class
        );

        Iterator<String> it = arr.fieldNames();
        while (it.hasNext()) {
            String value = it.next();
            if (!value.equals(newIndexName))
                restTemplate.delete(esUrl + "/" + value);
        }
    }
}