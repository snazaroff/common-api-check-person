package com.farzoom.uni.common.api.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.farzoom.uni.common.api.model")
@EntityScan(basePackages = {"com.farzoom.uni.common.api.model"})
public class Config {

    @Autowired
    private ApplicationContext applicationContext;

}
