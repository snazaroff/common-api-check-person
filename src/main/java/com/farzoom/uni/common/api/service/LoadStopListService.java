package com.farzoom.uni.common.api.service;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

@Slf4j
@Service
public class LoadStopListService extends LoadService {

    private final static SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd-hh-mm-ss");
    private final String inputFolder;
    private final String outputFolder;
    private final int maxRecordCount;

    public LoadStopListService(@Value("${folder.input.stop_list}") String inputFolder,
                               @Value("${record.count:1000}") int maxRecordCount, @Value("${es.url}") String esUrl,
                               @Value("${stop_list.alias}") String alias, @Value("${stop_list.type}") String type) {
        super(esUrl, alias, type);
        this.inputFolder = inputFolder;
        this.outputFolder = inputFolder + "/json";
        this.maxRecordCount = maxRecordCount;
    }

    public void process(String fileName) throws IOException {
        log.debug("processXlsxToJson(): " + fileName);

        long start = System.currentTimeMillis();
        File f = new File(outputFolder);
        if (!f.exists())
            f.mkdirs();

        String timeStamp = SDF.format(new Date());
        StringBuffer result = new StringBuffer();

        try (InputStream in = new FileInputStream(new File(fileName))) {
            XSSFWorkbook wb = new XSSFWorkbook(in);

            ObjectMapper om = new ObjectMapper();

            Sheet sheet = wb.getSheetAt(0);
            int rowCount = sheet.getLastRowNum();
            Iterator<Row> it = sheet.iterator();
            int rowNum = 0;
            int recordCount = 0;
            while (it.hasNext()) {
                rowNum++;
                Row row = it.next();
                if (rowNum < 2) continue;

                LoadData data = new LoadData();
                for (int i = 0; i < row.getLastCellNum(); i++) {
                    Cell cell = row.getCell(i);
                    String value;
                    if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
                        value = new String(cell.getStringCellValue().getBytes("cp1251")).trim();
                    } else if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
                        value = Integer.toString((int) cell.getNumericCellValue());
                    } else if (cell.getCellType() == Cell.CELL_TYPE_BLANK) {
                        value = "";
                    } else {
                        throw new RuntimeException("Wrong cell type: " + cell.getCellType());
                    }

                    if (value == null || value.equals("")) continue;
                    switch (i) {
                        //Уникальный идентификатор негативной истории
                        case 0:
                            data.setStopListId(value);
                            break;
                        //Уникальный идентификатор клиента (УИК)
                        case 1:
                            data.setClientId(value);
                            break;
                        //ИНН
                        case 2:
                            data.setInn(value);
                            break;
                        //КПП
                        case 3:
                            data.setKpp(value);
                            break;
                        //Полное наименование на русском языке
                        case 4:
                            data.setFullNameRus(value);
                            break;
                        //Сокращенное наименование на русском языке
                        case 5:
                            data.setShortNameRus(value);
                            break;
                        //Полное наименование на английском языке
                        case 6:
                            data.setFullNameEn(value);
                            break;
                        //Сокращенное наименование на английском языке
                        case 7:
                            data.setShortNameEn(value);
                            break;
                        //Код ОКПО
                        case 8:
                            data.setOkpo(value);
                            break;
                        //ОГРН
                        case 9:
                            data.setOgrn(value);
                            break;
                        //Код формы собственности по ОКФС
                        case 10:
                            data.setOkfs(value);
                            break;
                        //Код страны регистрации
                        case 11:
                            data.setCountry(value);
                            break;
                        //Код резидента/нерезидента
                        case 12:
                            data.setIsResident(value);
                            break;
                        //ИНН в стране постоянного пребывания - для нерезидентов
                        case 13:
                            data.setInnNotResident(value);
                            break;
                        //Тип стоп листа
                        case 14:
                            data.setStopListType(value);
                            break;
                        //Признак террориста
                        case 15:
                            data.setIsTerrorist(value);
                            break;
                        //Идентификатор файла
                        case 16:
                            data.setFileId(value);
                            break;
                        default:
                            throw new RuntimeException("Wrong xlsx file format");
                    }
                }
                result.append("{\"index\":{\"_id\": null}}");
                result.append("\n");
                result.append(om.writeValueAsString(data));
                result.append("\n");
                recordCount++;

                if (recordCount == maxRecordCount) {
                    String filePath = outputFolder + "/" + timeStamp + "." + UUID.randomUUID().toString() + ".json";
                    try (FilterOutputStream os = new PrintStream(new FileOutputStream(new File(filePath)), true, "UTF-8")) {
                        os.write(result.toString().getBytes("UTF-8"));
                    } catch (Exception e) {
                        log.error(e.getMessage(), e);
                    }
                    processJsonToEs(filePath);
                    result = new StringBuffer();
                    recordCount = 0;
//                    break;
                }
//                break;
            }

            //Сохраняем данные в файл
            String filePath = outputFolder + "/" + timeStamp + "." + UUID.randomUUID().toString() + ".json";
            try (FilterOutputStream os = new PrintStream(new FileOutputStream(new File(filePath)), true, "UTF-8")) {
                os.write(result.toString().getBytes("UTF-8"));
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
            processJsonToEs(filePath);

            File processed = new File(inputFolder + "/processed/");
            if (!processed.exists())
                processed.mkdirs();

            //Перемещаем файл в обработанные
            Files.move(Paths.get(fileName), Paths.get(inputFolder + "/processed/" + new File(fileName).getName()), REPLACE_EXISTING);

            removeOldIndex(alias, alias + "-" + timeStamp);
        }
        log.debug("Load: " + fileName + "; time: " + (System.currentTimeMillis() - start) + " ms");
    }

    @Setter
    @Getter
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private class LoadData {

        @JsonProperty("stop_list_id")
        private String stopListId;
        @JsonProperty("client_id")
        private String clientId;
        @JsonProperty("inn")
        private String inn;
        @JsonProperty("kpp")
        private String kpp;
        @JsonProperty("full_name_rus")
        private String fullNameRus;
        @JsonProperty("short_name_rus")
        private String shortNameRus;
        @JsonProperty("full_name_en")
        private String fullNameEn;
        @JsonProperty("short_name_en")
        private String shortNameEn;
        @JsonProperty("okpo")
        private String okpo;
        @JsonProperty("ogrn")
        private String ogrn;
        @JsonProperty("okfs")
        private String okfs;
        @JsonProperty("country")
        private String country;
        @JsonProperty("is_resident")
        private String isResident;
        @JsonProperty("inn_not_resident")
        private String innNotResident;
        @JsonProperty("stop_list_type")
        private String stopListType;
        @JsonProperty("is_terrorist")
        private String isTerrorist;
        @JsonProperty("file_id")
        private String fileId;
    }

}