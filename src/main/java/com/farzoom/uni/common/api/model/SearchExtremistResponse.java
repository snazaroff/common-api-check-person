package com.farzoom.uni.common.api.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

@Setter
@Getter
@ToString
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class SearchExtremistResponse {

    @JsonProperty("type")
    Integer type;
    @JsonProperty("name")
    String name;
    @JsonProperty("country")
    String country;
    @JsonProperty("address")
    String address;
    @JsonProperty("passportSeries")
    String passportSeries;
    @JsonProperty("passportNumber")
    String passportNumber;
    @JsonProperty("birthDate")
    @JsonFormat(shape= JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    Date birthDate;
    @JsonProperty("birthPlace")
    String birthPlace;
    @JsonProperty("inn")
    String inn;
    @JsonProperty("_scope")
    Double scope;
}
