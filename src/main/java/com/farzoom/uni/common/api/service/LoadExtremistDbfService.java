package com.farzoom.uni.common.api.service;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.jamel.dbf.DbfReader;
import org.jamel.dbf.structure.DbfField;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

@Slf4j
@Service
public class LoadExtremistDbfService extends LoadService {

    private final static SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd-hh-mm-ss");
    private final String inputFolder;
    private final String outputFolder;
    private final int maxRecordCount;

    public LoadExtremistDbfService(@Value("${folder.input.extremist}") String inputFolder,
                                @Value("${record.count:1000}") int maxRecordCount, @Value("${es.url}") String esUrl,
                                @Value("${terror.alias}") String alias, @Value("${terror.type}") String type) {
        super(esUrl, alias, type);
        this.inputFolder = inputFolder;
        this.outputFolder = inputFolder + "/json";
        this.maxRecordCount = maxRecordCount;
    }

    public void process(String fileName) throws IOException {
        log.debug("process(): " + fileName);

        long start = System.currentTimeMillis();
        File f = new File(outputFolder);
        if (!f.exists())
            f.mkdirs();

        String timeStamp = (new SimpleDateFormat("yyyy-MM-dd-hh-mm-ss")).format(new Date());

        StringBuffer result = new StringBuffer();
        try (DbfReader reader = new DbfReader(new File(fileName))) {

            List<DbfField> fieldList = new ArrayList<>();
            for (int i = 0 ; i < reader.getHeader().getFieldsCount(); i++) {
                fieldList.add(reader.getHeader().getField(i));
            }

            Object[] row;
            int recordCount = 0;
            ObjectMapper om = new ObjectMapper();
            LoadData prevData = null;
            while ((row = reader.nextRecord()) != null) {

                LoadData data = new LoadData();
                for(int i = 0; i < fieldList.size(); i++) {
                    DbfField field = fieldList.get(i);
                    if (field.getName().equals("TU")) {
                        Number value = (Number) row[i];
                        data.setType(value.intValue());
                    } else if (field.getName().equals("NAMEU")) {
                        String value = new String(((byte[]) row[i]), "Cp866").trim();
                        data.setName(value);
                    } else if (field.getName().equals("KODCN")) {
                        String value = new String(((byte[]) row[i]), "Cp866").trim();
                        data.setCountry(value);
                    } else if (field.getName().equals("ADRESS")) {
                        String value = new String(((byte[]) row[i]), "Cp866").trim();
                        data.setAddress(value);
                    } else if (field.getName().equals("SD")) {
                        String value = new String(((byte[]) row[i]), "Cp866").trim();
                        data.setPassportSeries(value);
                    } else if (field.getName().equals("RG")) {
                        String value = new String(((byte[]) row[i]), "Cp866").trim();
                        data.setPassportNumber(value);
                    } else if (field.getName().equals("GR")) {
                        Date value = (Date) row[i];
                        if (value.getTime() > -30000000000000L)
                            data.setBirthDate(value);
                    } else if (field.getName().equals("MR")) {
                        String value = new String(((byte[]) row[i]), "Cp866").trim();
                        data.setBirthPlace(value);
                    } else if (field.getName().equals("ND")) {
                        String value = new String(((byte[]) row[i]), "Cp866").trim();
                        data.setInn(value);
                    }
                }
                if (prevData != null) {
                    if (data.getType() == 0) {
                        prevData.merge(data);
                    } else {
                        result.append("{\"index\":{\"_id\": null}}");
                        result.append("\n");
                        result.append(om.writeValueAsString(prevData));
                        result.append("\n");
                        prevData = data;
                        recordCount++;
                    }
                } else {
                    prevData = data;
                }

                if (recordCount == maxRecordCount) {
                    String filePath = outputFolder + "/" + timeStamp + "." + UUID.randomUUID().toString() + ".json";
                    try (FilterOutputStream os = new PrintStream(new FileOutputStream(new File(filePath)), true, "UTF-8")) {
                        os.write(result.toString().getBytes("UTF-8"));
                    } catch( Exception e) {
                        log.error(e.getMessage(), e);
                    }
                    processJsonToEs(filePath);
                    result = new StringBuffer();
                    recordCount = 0;
//                    break;
                }
//                break;
            }
            if (prevData != null) {
                result.append("{\"index\":{\"_id\": null}}");
                result.append("\n");
                result.append(om.writeValueAsString(prevData));
            }
        }

        String filePath = outputFolder + "/" + timeStamp + "." + UUID.randomUUID().toString() + ".json";
        try (FilterOutputStream os = new PrintStream(new FileOutputStream(new File(filePath)), true, "UTF-8")) {
            os.write(result.toString().getBytes("UTF-8"));
        } catch( Exception e) {
            log.error(e.getMessage(), e);
        }
        processJsonToEs(filePath);

        File processed = new File(inputFolder + "/processed/");
        if (!processed.exists())
            processed.mkdirs();

        Files.move(Paths.get(fileName), Paths.get(inputFolder + "/processed/" + new File(fileName).getName()), REPLACE_EXISTING);

        removeOldIndex(alias, alias + "-" + timeStamp);

        log.debug("Load: " + fileName + "; time: " + (System.currentTimeMillis() - start) + " ms");
    }

    @Setter
    @Getter
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private class LoadData {

//    type: TU, //1 - ФЛ, 3 - ЮЛ. !!! 0 -> продолжение предыдущей строки
//    names: NAMEU,
//    country: KODCN,
//    address: ADRESS,
//    passport:{series: SD, number: RG},
//    birthDate: GR,
//    birthPlace: MR,
//    inn: ND

        @JsonProperty("type")
        Integer type;
        @JsonProperty("name")
        String name;
        @JsonProperty("country")
        String country;
        @JsonProperty("address")
        String address;
        @JsonProperty("passportSeries")
        String passportSeries;
        @JsonProperty("passportNumber")
        String passportNumber;
        @JsonProperty("birthDate")
        @JsonFormat(shape= JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
        Date birthDate;
        @JsonProperty("birthPlace")
        String birthPlace;
        @JsonProperty("inn")
        String inn;

        public void merge(LoadData data) {
            this.name = this.name.concat(data.name);
        }
    }
}
