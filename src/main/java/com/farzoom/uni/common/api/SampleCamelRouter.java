package com.farzoom.uni.common.api;

import com.farzoom.uni.common.api.service.LoadExtremistDbfService;
import com.farzoom.uni.common.api.service.LoadExtremistXlsService;
import com.farzoom.uni.common.api.service.LoadStopListService;
import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class SampleCamelRouter extends RouteBuilder {

    private final String extremistInputFolder;
    private final String stopListInputFolder;

    private final LoadExtremistXlsService loadExtremistXlsService;
    private final LoadExtremistDbfService loadExtremistDbfService;
    private final LoadStopListService loadStopListService;

    public SampleCamelRouter(LoadExtremistXlsService loadExtremistXlsService,
                             LoadExtremistDbfService loadExtremistDbfService,
                             LoadStopListService loadStopListService,
                             @Value("${folder.input.extremist}") String extremistInputFolder,
                             @Value("${folder.input.stop_list}") String stopListInputFolder
    ) {
        this.loadExtremistXlsService = loadExtremistXlsService;
        this.loadExtremistDbfService = loadExtremistDbfService;
        this.loadStopListService = loadStopListService;
        this.extremistInputFolder = extremistInputFolder;
        this.stopListInputFolder = stopListInputFolder;
    }

    @Override
    public void configure() throws Exception {

        //Запуск теста. Видно, что camel запущен
        from("timer:hello?period={{timer.period}}")
                .transform(method("myBean", "saySomething"))
                .to("stream:out");

        //delay
        //Запускает обработку списка экстремистов
        from("file:" + extremistInputFolder + "?delete=true&readLock=rename&delay=5000")
                .setHeader(Exchange.FILE_NAME, simple("${file:name.noext}-${date:now:yyyyMMddHHmmssSSS}.${file:ext}"))
                .to("file:" + extremistInputFolder + "/ready_for_processed/")
                .setHeader("input", constant(extremistInputFolder + "/ready_for_processed"))
                .bean(loadExtremistXlsService, "process(${header.input}/${header.CamelFileName})");

        //Запускает обработку списка экстремистов
        from("file:" + extremistInputFolder + "/dbf?delete=true&readLock=rename&delay=5000")
                .setHeader(Exchange.FILE_NAME, simple("${file:name.noext}-${date:now:yyyyMMddHHmmssSSS}.${file:ext}"))
                .to("file:" + extremistInputFolder + "/ready_for_processed/")
                .setHeader("input", constant(extremistInputFolder + "/ready_for_processed"))
                .bean(loadExtremistDbfService, "process(${header.input}/${header.CamelFileName})");

        //Запускает обработку стоп листа
        from("file:" + stopListInputFolder + "?delete=true&readLock=rename")
                .setHeader(Exchange.FILE_NAME, simple("${file:name.noext}-${date:now:yyyyMMddHHmmssSSS}.${file:ext}"))
                .to("file:" + stopListInputFolder + "/ready_for_processed/")
                .setHeader("input", constant(stopListInputFolder + "/ready_for_processed"))
                .bean(loadStopListService, "process(${header.input}/${header.CamelFileName})");

    }

}
