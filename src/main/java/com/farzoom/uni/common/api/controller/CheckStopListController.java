package com.farzoom.uni.common.api.controller;

import com.farzoom.uni.common.api.model.SearchStopListRequest;
import com.farzoom.uni.common.api.model.SearchStopListResponse;
import com.farzoom.uni.common.api.service.CheckStopListService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/stop_list")
public class CheckStopListController {

    private final CheckStopListService checkStopListService;

    public CheckStopListController(CheckStopListService searchService) {
        this.checkStopListService = searchService;
    }

    @ApiOperation(notes = "", value = "", nickname = "doSearch",
            tags = {"Start search"} )
    @RequestMapping(path = "/search", method = RequestMethod.POST, produces = {"application/json"})
    public List<SearchStopListResponse> search(@RequestBody @Valid SearchStopListRequest request) throws Exception {
        log.debug("CheckStopListController.search ");
        return checkStopListService.search(request);
    }
}