package com.farzoom.uni.common.api.service;

import com.farzoom.uni.common.api.NotFoundException;
import com.farzoom.uni.common.api.model.SearchExtremistRequest;
import com.farzoom.uni.common.api.model.SearchExtremistResponse;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.*;

@Slf4j
@Service
public class CheckExtremistService {

    private final String esUrl;
    private final String alias;
    private final String type;
    private final RestTemplate restTemplate;

    public CheckExtremistService(@Value("${es.url}") String esUrl, @Value("${terror.alias}") String alias,
                                 @Value("${terror.type}") String type) {
        this.esUrl = esUrl;
        this.alias = alias;
        this.type = type;
        this.restTemplate = new RestTemplate();
    }

    public List<SearchExtremistResponse> search(SearchExtremistRequest searchRequest) throws Exception {
        log.debug("search(): " + searchRequest);

//        {
//            "query": {
//                "bool": {
//                    "filter": [
//                        { "match": { "name": "АБАДИЕВ" } },
//                        { "match": { "name": "МУСА" } },
//                        { "match": { "birthDate": "1990-05-08" } },
//                        { "term": { "country": "643" } }
//                    ]
//                }
//            }
//        }

        //Формируем query
        ObjectMapper mapper = new ObjectMapper();
        ArrayNode arrayNode = mapper.createArrayNode();

        if (searchRequest.getName() != null && !searchRequest.getName().equals("")) {
            String[] names = searchRequest.getName().split(" ");
            for(String name: names) {
                ObjectNode on = mapper.createObjectNode();
                on.set("match", mapper.createObjectNode().put("name", name));
                arrayNode.add(on);
            }
        }
        if (searchRequest.getCountry() != null && !searchRequest.getCountry().equals("")) {
            ObjectNode on = mapper.createObjectNode();
            on.set("term", mapper.createObjectNode().put("country", searchRequest.getCountry()));
            arrayNode.add(on);
        }
        if (searchRequest.getBirthDate() != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            ObjectNode on = mapper.createObjectNode();
            on.set("match", mapper.createObjectNode().put("birthDate", sdf.format(searchRequest.getBirthDate())));
            arrayNode.add(on);
        }
        if (searchRequest.getBirthPlace() != null && !searchRequest.getBirthPlace().equals("")) {
            ObjectNode on = mapper.createObjectNode();
            on.set("match", mapper.createObjectNode().put("birthPlace", searchRequest.getBirthPlace()));
            arrayNode.add(on);
        }
        if (searchRequest.getType() != null && !searchRequest.getType().equals("")) {
            ObjectNode on = mapper.createObjectNode();
            on.set("term", mapper.createObjectNode().put("type", searchRequest.getType()));
            arrayNode.add(on);
        }
        if (searchRequest.getInn() != null && !searchRequest.getInn().equals("")) {
            ObjectNode on = mapper.createObjectNode();
            on.set("term", mapper.createObjectNode().put("inn", searchRequest.getInn()));
            arrayNode.add(on);
        }
        if (searchRequest.getPassportSeries() != null && !searchRequest.getPassportSeries().equals("")) {
            ObjectNode on = mapper.createObjectNode();
            on.set("term", mapper.createObjectNode().put("passportSeries", searchRequest.getPassportSeries()));
            arrayNode.add(on);
        }
        if (searchRequest.getPassportNumber() != null && !searchRequest.getPassportNumber().equals("")) {
            ObjectNode on = mapper.createObjectNode();
            on.set("term", mapper.createObjectNode().put("passportNumber", searchRequest.getPassportNumber()));
            arrayNode.add(on);
        }

        if (arrayNode.size() == 0)
            throw new NotFoundException("Пустой запрос");

        JsonNode jn = mapper
                .createObjectNode()
                .set("query", mapper
                        .createObjectNode()
                        .set("bool", mapper
                                .createObjectNode().set("filter", arrayNode)));

        String query = jn.toString();
        log.debug("query: " + query);

        //Делаем запрос
        HttpHeaders headers = new HttpHeaders();
        MediaType mediaType = new MediaType("application", "json", StandardCharsets.UTF_8);
        headers.setContentType(mediaType);

        HttpEntity<String> request = new HttpEntity<>(query, headers);
        log.debug(esUrl + "/" + alias + "/" + type + "/_search");

        List<JsonNode> objs = restTemplate.postForObject(
                esUrl + "/" + alias + "/" + type + "/_search",
                request,
                ObjectNode.class
        ).findParents("_source");

        //Разбираем результат
        List<SearchExtremistResponse> result = new ArrayList<>();
        for(JsonNode o: objs) {
            log.debug("_source: " + o.findValue("_source").toString());
            log.debug("_score: " + o.findValue("_score").toString());
            SearchExtremistResponse response = mapper.readValue(o.findValue("_source").toString(), SearchExtremistResponse.class);
            response.setScope(o.findValue("_score").asDouble());
            result.add(response);
        }
        return result;
    }
}