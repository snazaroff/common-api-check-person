package com.farzoom.uni.common.api.controller;

import com.farzoom.uni.common.api.model.SearchExtremistRequest;
import com.farzoom.uni.common.api.model.SearchExtremistResponse;
import com.farzoom.uni.common.api.service.CheckExtremistService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/extremist")
public class CheckExtremistController {

    private final CheckExtremistService searchService;

    public CheckExtremistController(CheckExtremistService searchService) {
        this.searchService = searchService;
    }

    @ApiOperation(notes = "", value = "", nickname = "doSearch",
            tags = {"Start search"} )
    @RequestMapping(path = "/search", method = RequestMethod.POST, produces = {"application/json"})
    public List<SearchExtremistResponse> search(@RequestBody @Valid SearchExtremistRequest request) throws Exception {
        log.debug("CheckExtremistController.search ");
        return searchService.search(request);
    }
}