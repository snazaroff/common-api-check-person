package com.farzoom.uni.common.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class SearchStopListRequest {

    @JsonProperty("inn")
    private String inn;
    @JsonProperty("kpp")
    private String kpp;
    @JsonProperty("full_name_rus")
    private String fullNameRus;
    @JsonProperty("short_name_rus")
    private String shortNameRus;
    @JsonProperty("full_name_en")
    private String fullNameEn;
    @JsonProperty("short_name_en")
    private String shortNameEn;
    @JsonProperty("okpo")
    private String okpo;
    @JsonProperty("ogrn")
    private String ogrn;
    @JsonProperty("okfs")
    private String okfs;
    @JsonProperty("country")
    private String country;
}
