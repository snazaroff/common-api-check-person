package com.farzoom.uni.common.api.controller;

import com.farzoom.uni.common.api.service.LoadStopListService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;

@Slf4j
@RestController
@RequestMapping("/stop_list")
public class LoadStopListController {

    private final LoadStopListService loadStopListService;

    public LoadStopListController(LoadStopListService loadStopListService) {
        this.loadStopListService = loadStopListService;
    }

    @ApiOperation(notes = "", value = "", nickname = "doLoading",
            tags = {"Load data"} )
    @RequestMapping(path = "/xlsx", method = RequestMethod.POST, produces = {"application/vnd.ms-excel"})
    public String loadStopList(@RequestParam("file") MultipartFile file) throws IOException {
        log.debug("LoadStopListController.loadStopList ");

        String filePath = "load_" + UUID.randomUUID() + file.getName();
        File tmpFile = File.createTempFile(filePath, "");
        if (!file.isEmpty()) {
            try {
                byte[] bytes = file.getBytes();
                BufferedOutputStream stream =
                        new BufferedOutputStream(new FileOutputStream(tmpFile));
                stream.write(bytes);
                stream.close();
                loadStopListService.process(tmpFile.getAbsolutePath());

                return tmpFile.getName();
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                return "You failed to upload " + tmpFile.getName() + " => " + e.getMessage();
            }
        } else {
            return "You failed to upload " + tmpFile.getName() + " because the file was empty.";
        }
    }
}