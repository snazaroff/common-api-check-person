package com.farzoom.uni.common.api.service;

import com.farzoom.uni.common.api.NotFoundException;
import com.farzoom.uni.common.api.model.SearchStopListRequest;
import com.farzoom.uni.common.api.model.SearchStopListResponse;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class CheckStopListService {

    private final String esUrl;
    private final String alias;
    private final String type;
    private final RestTemplate restTemplate;

    public CheckStopListService(@Value("${es.url}") String esUrl, @Value("${stop_list.alias}") String alias,
                                @Value("${stop_list.type}") String type) {
        this.esUrl = esUrl;
        this.alias = alias;
        this.type = type;
        this.restTemplate = new RestTemplate();
    }

    public List<SearchStopListResponse> search(SearchStopListRequest searchRequest) throws Exception {
        log.debug("search(): " + searchRequest);

//        {
//            "query": {
//                "bool": {
//                    "filter": [
//                        { "match": { "name": "АБАДИЕВ" } },
//                        { "match": { "name": "МУСА" } },
//                        { "match": { "birthDate": "1990-05-08" } },
//                        { "term": { "country": "643" } }
//                    ]
//                }
//            }
//        }

        //Формируем query
        ObjectMapper mapper = new ObjectMapper();
        ArrayNode arrayNode = mapper.createArrayNode();

        if (searchRequest.getFullNameRus() != null && !searchRequest.getFullNameRus().equals("")) {
            String[] names = searchRequest.getFullNameRus().split(" ");
            for(String name: names) {
                ObjectNode on = mapper.createObjectNode();
                on.set("match", mapper.createObjectNode().put("full_name_rus", name));
                arrayNode.add(on);
            }
        }
        if (searchRequest.getShortNameRus() != null && !searchRequest.getShortNameRus().equals("")) {
            String[] names = searchRequest.getShortNameRus().split(" ");
            for(String name: names) {
                ObjectNode on = mapper.createObjectNode();
                on.set("match", mapper.createObjectNode().put("short_name_rus", name));
                arrayNode.add(on);
            }
        }
        if (searchRequest.getFullNameEn() != null && !searchRequest.getFullNameEn().equals("")) {
            String[] names = searchRequest.getFullNameEn().split(" ");
            for(String name: names) {
                ObjectNode on = mapper.createObjectNode();
                on.set("match", mapper.createObjectNode().put("full_name_en", name));
                arrayNode.add(on);
            }
        }
        if (searchRequest.getShortNameEn() != null && !searchRequest.getShortNameEn().equals("")) {
            String[] names = searchRequest.getShortNameEn().split(" ");
            for(String name: names) {
                ObjectNode on = mapper.createObjectNode();
                on.set("match", mapper.createObjectNode().put("short_name_en", name));
                arrayNode.add(on);
            }
        }
        if (searchRequest.getCountry() != null && !searchRequest.getCountry().equals("")) {
            ObjectNode on = mapper.createObjectNode();
            on.set("term", mapper.createObjectNode().put("country", searchRequest.getCountry()));
            arrayNode.add(on);
        }
        if (searchRequest.getInn() != null && !searchRequest.getInn().equals("")) {
            ObjectNode on = mapper.createObjectNode();
            on.set("term", mapper.createObjectNode().put("inn", searchRequest.getInn()));
            arrayNode.add(on);
        }
        if (searchRequest.getKpp() != null && !searchRequest.getKpp().equals("")) {
            ObjectNode on = mapper.createObjectNode();
            on.set("term", mapper.createObjectNode().put("kpp", searchRequest.getKpp()));
            arrayNode.add(on);
        }
        if (searchRequest.getOkpo() != null && !searchRequest.getOkpo().equals("")) {
            ObjectNode on = mapper.createObjectNode();
            on.set("term", mapper.createObjectNode().put("okpo", searchRequest.getOkpo()));
            arrayNode.add(on);
        }
        if (searchRequest.getOgrn() != null && !searchRequest.getOgrn().equals("")) {
            ObjectNode on = mapper.createObjectNode();
            on.set("term", mapper.createObjectNode().put("ogrn", searchRequest.getOgrn()));
            arrayNode.add(on);
        }
        if (searchRequest.getOkfs() != null && !searchRequest.getOkfs().equals("")) {
            ObjectNode on = mapper.createObjectNode();
            on.set("term", mapper.createObjectNode().put("okfs", searchRequest.getOkfs()));
            arrayNode.add(on);
        }

        if (arrayNode.size() == 0)
            throw new NotFoundException("Пустой запрос");

        JsonNode jn = mapper
                .createObjectNode()
                .set("query", mapper
                        .createObjectNode()
                        .set("bool", mapper
                                .createObjectNode().set("filter", arrayNode)));

        String query = jn.toString();
        log.debug("query: " + query);

        //Делаем запрос
        HttpHeaders headers = new HttpHeaders();
        MediaType mediaType = new MediaType("application", "json", StandardCharsets.UTF_8);
        headers.setContentType(mediaType);

        HttpEntity<String> request = new HttpEntity<>(query, headers);

        List<JsonNode> objs = restTemplate.postForObject(
                esUrl + "/" + alias + "/" + type + "/_search",
                request,
                ObjectNode.class
        ).findParents("_source");

        //Разбираем результат
        List<SearchStopListResponse> result = new ArrayList<>();
        for(JsonNode o: objs) {
            SearchStopListResponse response = mapper.readValue(o.findValue("_source").toString(), SearchStopListResponse.class);
            response.setScope(o.findValue("_score").asDouble());
            result.add(response);
        }
        return result;
    }
}