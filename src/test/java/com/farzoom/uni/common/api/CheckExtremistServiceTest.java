package com.farzoom.uni.common.api;

import com.farzoom.uni.common.api.model.SearchExtremistRequest;
import com.farzoom.uni.common.api.model.SearchExtremistResponse;
import com.farzoom.uni.common.api.service.CheckExtremistService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.SimpleDateFormat;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Slf4j
public class CheckExtremistServiceTest {

    @Autowired
    private CheckExtremistService checkExtremistService;

    @Test
    public void test() throws Exception {

//        {
//            "query": {
//                "bool": {
//                    "filter": [
//                        { "match": { "name": "АБАДИЕВ" } },
//                        { "match": { "name": "МУСА" } },
//                        { "match": { "birthDate": "1990-05-08" } },
//                        { "term": { "country": "643" } }
//                    ]
//                }
//            }
//        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        SearchExtremistRequest request = new SearchExtremistRequest();
//        request.setName("АБАДИЕВ МУСА");
//        request.setBirthDate(sdf.parse("1990-05-08"));
        request.setCountry("643");
        log.debug("request: " + request);
        List<SearchExtremistResponse> response = checkExtremistService.search(request);
        log.debug("response: " + response);
        Assert.assertNotEquals(0, response.size());
    }

    @Test(expected = NotFoundException.class)
    public void testException() throws Exception {
        SearchExtremistRequest request = new SearchExtremistRequest();
        log.debug("request: " + request);
        List<SearchExtremistResponse> response = checkExtremistService.search(request);
        log.debug("response: " + response);
    }
}