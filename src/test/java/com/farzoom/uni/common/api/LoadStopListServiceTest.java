package com.farzoom.uni.common.api;

import com.farzoom.uni.common.api.service.LoadStopListService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Slf4j
public class LoadStopListServiceTest {

    @Autowired
    private LoadStopListService loadStopListService;

    @Test
    public void test() throws IOException {
        final String fileName = "stop_list.xlsx";
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream mIs = classloader.getResourceAsStream(fileName);

        int size = IOUtils.copy(mIs, new FileOutputStream(new File(fileName)));
        log.debug("file.size: " + size);
        mIs.close();

        loadStopListService.process(fileName);
    }
}