package com.farzoom.uni.common.api;

import com.farzoom.uni.common.api.model.SearchStopListRequest;
import com.farzoom.uni.common.api.model.SearchStopListResponse;
import com.farzoom.uni.common.api.service.CheckStopListService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.SimpleDateFormat;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Slf4j
public class CheckStopListServiceTest {

    @Autowired
    private CheckStopListService checkStopListService;

    @Test
    public void test() throws Exception {

//        {
//            "query": {
//                "bool": {
//                    "filter": [
//                        { "match": { "name": "АБАДИЕВ" } },
//                        { "match": { "name": "МУСА" } },
//                        { "match": { "birthDate": "1990-05-08" } },
//                        { "term": { "country": "643" } }
//                    ]
//                }
//            }
//        }

        SearchStopListRequest request = new SearchStopListRequest();
        request.setFullNameRus("МЕРКУРИЙ");
        log.debug("request: " + request);
        List<SearchStopListResponse> response = checkStopListService.search(request);
        log.debug("response: " + response);
        Assert.assertNotEquals(0, response.size());
    }
}