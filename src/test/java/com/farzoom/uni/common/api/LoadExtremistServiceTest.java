package com.farzoom.uni.common.api;

import com.farzoom.uni.common.api.service.LoadExtremistXlsService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.*;
import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Slf4j
public class LoadExtremistServiceTest {

    @Autowired
    private LoadExtremistXlsService loadExtremistXlsService;

    @Test
    public void test() throws IOException {

        final String fileName = "test.xlsx";

        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream mIs = classloader.getResourceAsStream(fileName);

        String filePath = "test" + UUID.randomUUID() + ".xlsx";
        OutputStream os = new FileOutputStream(new File(filePath));
        int size = IOUtils.copy(mIs, os);
        log.debug("file.size: " + size);
        mIs.close();
        os.close();

        loadExtremistXlsService.process(filePath);
    }
}