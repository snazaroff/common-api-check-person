package com.farzoom.uni.common.api;

import com.farzoom.uni.common.api.model.SearchExtremistRequest;
import com.farzoom.uni.common.api.model.SearchExtremistResponse;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import java.text.SimpleDateFormat;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
@WebAppConfiguration
public class CheckExtremistControllerTest {

    protected MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setup() throws Exception {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void test() throws Exception {

//        {
//            "query": {
//                "bool": {
//                    "filter": [
//                        { "match": { "name": "АБАДИЕВ" } },
//                        { "match": { "name": "МУСА" } },
//                        { "match": { "birthDate": "1990-05-08" } },
//                        { "term": { "country": "643" } }
//                    ]
//                }
//            }
//        }

//        {
//            "birthDate": "1990-05-08",
//                "country": "643",
//                "name": "АБАДИЕВ МУСА"
//        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        SearchExtremistRequest request = new SearchExtremistRequest();
//        request.setName("АБАДИЕВ МУСА");
//        request.setBirthDate(sdf.parse("1990-05-08"));
        request.setCountry("643");
        log.debug("request: " + request);

        ObjectMapper om = new ObjectMapper();
        MvcResult mr = this.mockMvc
                .perform(post("/extremist/search")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(om.writeValueAsString(request))
                )
                .andExpect(status().isOk())
                .andReturn();

        List<SearchExtremistResponse> list = om.readValue(mr.getResponse().getContentAsByteArray(), new TypeReference<List<SearchExtremistResponse>>(){});
        log.debug("list: " + om.writeValueAsString(list));
        Assert.assertNotEquals(0, list.size());
    }

    @Test
    public void testException() throws Exception {
        SearchExtremistRequest request = new SearchExtremistRequest();
        log.debug("request: " + request);

        ObjectMapper om = new ObjectMapper();
        MvcResult mr = this.mockMvc
                .perform(post("/extremist/search")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(om.writeValueAsString(request))
                )
                .andExpect(status().isNotFound())
                .andReturn();
    }
}